package com.teknei.edicom.ecs.nom151;

import com.teknei.edicom.ecs.nom151.client.Enom151Client;
import com.teknei.edicom.ecs.nom151.client.Enom151Exception;
import com.teknei.edicom.ecs.nom151.helper.FileUtils;
import com.teknei.edicom.ecs.nom151.helper.GestorLogs;

import com.sanityinc.jargs.CmdLineParser;
import com.sanityinc.jargs.CmdLineParser.Option;
import com.sanityinc.jargs.CmdLineParser.OptionException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class GetConstancia
  extends Enom151Program
{
  GetConstancia() throws Enom151Exception
  {}
  private static final Logger log = LoggerFactory.getLogger(GetConstancia.class);
  public void showHelp()
  {
	  log.info("lblancas:  [tkn-nom-151] :: "+this.getClass().getName()+".showHelp");
    StringBuilder buffer = new StringBuilder();
    buffer.append("GetConstancia: \n");
    buffer.append("\n");
    buffer.append("How to use: GetConstancia ");
    buffer.append("[{-").append('i').append(", --").append("in").append("} dirIn").append(", ");
    buffer.append("[{-").append('o').append(", --").append("out").append("} dirOut").append(", ");
    
    buffer.append("{-").append('u').append(", --").append("user").append("} user").append(", ");
    buffer.append("{-").append('p').append(", --").append("pass").append("} pass").append(", ");
    
    buffer.append("{-").append('f').append(", --").append("file").append("} file").append(", ");
    buffer.append("{-").append('e').append(", --").append("idExt").append("} idExt").append(", ");
    
    buffer.append("{-").append('r').append(", --").append("deleteInputFiles").append("} deleteInputFiles]");
    
    GestorLogs.info(GetConstancia.class, "Help", buffer.toString());
  }
  
  void addArguments(String[] args)
    throws Enom151Exception
  {
	  log.info("lblancas:  [tkn-nom-151] :: "+this.getClass().getName()+".addArguments");
    CmdLineParser parser = new CmdLineParser();
    
    Option<String> url = parser.addStringOption('l', "url");
    Option<String> pfx = parser.addStringOption('s', "pfx");
    Option<String> key = parser.addStringOption('k', "key");
    
    Option<String> dirIn = parser.addStringOption('i', "in");
    Option<String> dirOut = parser.addStringOption('o', "out");
    
    Option<String> user = parser.addStringOption('u', "user");
    Option<String> pass = parser.addStringOption('p', "pass");
    
    Option<String> file = parser.addStringOption('f', "file");
    Option<String> idExt = parser.addStringOption('e', "idExt");
    
    Option<String> delete = parser.addStringOption('r', "deleteInputFiles");
    try
    {
      parser.parse(args, Locale.getDefault());
    } catch (OptionException optionException) {
      String texto = "Error while processing client parameters: ";
      GestorLogs.error(GetConstancia.class, "Add Arguments", texto + optionException.toString(), optionException);
      showHelp();
    }
    
    String urlValue = (String)parser.getOptionValue(url);
    insertaValorProperties("url", urlValue);
    
    String pfxValue = (String)parser.getOptionValue(pfx);
    insertaValorProperties("pfx", pfxValue);
    
    String keyValue = (String)parser.getOptionValue(key);
    insertaValorProperties("key", keyValue);
    
    String dirInValue = (String)parser.getOptionValue(dirIn);
    insertaValorProperties("in", dirInValue);
    
    String dirOutValue = (String)parser.getOptionValue(dirOut);
    insertaValorProperties("out", dirOutValue);
    
    String userValue = (String)parser.getOptionValue(user);
    insertaValorProperties("user", userValue);
    
    String passValue = (String)parser.getOptionValue(pass);
    insertaValorProperties("pass", passValue);
    
    String fileValue = (String)parser.getOptionValue(file);
    insertaValorProperties("file", fileValue);
    
    String idExtValue = (String)parser.getOptionValue(idExt);
    insertaValorProperties("idExt", idExtValue);
    
    String deleteValue = (String)parser.getOptionValue(delete);
    insertaValorProperties("deleteInputFiles", deleteValue);
  }
  
  boolean validaDatos() throws Enom151Exception
  {
	  log.info("lblancas:  [tkn-nom-151] :: "+this.getClass().getName()+"validaDatos");
    boolean validateIn = false;
    boolean validateOut = false;
    try {
      String dirIn = getProperties().getProperty("in");
      File f = new File(dirIn);
      String file = getProperties().getProperty("file");
      if (((f.exists()) && (f.list().length > 0)) || (StringUtils.isNotBlank(file))) {
        validateIn = true;
      }
      String outDir = getProperties().getProperty("out");
      f = new File(outDir);
      if (f.exists()) {
        validateOut = true;
      }
    } catch (Exception exception) {
      GestorLogs.error(GetConstancia.class, "Exception", exception.getMessage(), exception);
    }
    return (validateIn) && (validateOut);
  }
  


  protected Map<String, byte[]> getConstancia()
    throws Enom151Exception
  {
	  log.info("lblancas:  [tkn-nom-151] :: "+this.getClass().getName()+".getConstancia");
    Map<String, byte[]> result = new HashMap<String, byte[]>();
    GestorLogs.info(GetConstancia.class, "GET CONSTANCIA", "start");
    
    try
    {
      Enom151Client client = new Enom151Client(getProperties().getProperty("url"), getProperties().getProperty("user"), getProperties().getProperty("pass"));
      
      Map<String, String> filesHash = new HashMap<String, String>();
      String fileParameter = getProperties().getProperty("file");
      String dirIn = getProperties().getProperty("in");
      File fDirIn = new File(dirIn);
      if ((StringUtils.isBlank(fileParameter)) && (fDirIn.exists()) && (fDirIn.list().length > 0)) {
        filesHash = processFilesHash(fDirIn);
      }
      if (StringUtils.isNotBlank(fileParameter)) {
        File fParameter = new File(fileParameter);
        if (fParameter.exists()) {
          filesHash.put(fileParameter, FileUtils.getSha256Timestamp(FileUtils.leerFichero(fileParameter)));
        } else {
          filesHash = processFilesHash(fDirIn, fileParameter);
        }
      }
      if (filesHash.isEmpty()) {
        throw new Enom151Exception("Error en la interpretación de los parámetros de entrada");
      }
      
      String idExt = getProperties().getProperty("idExt");
      
      for (Map.Entry<String, String> entry : filesHash.entrySet())
        result.put(entry.getKey(), client.getConstanciaHash((String)entry.getValue(), idExt));
    } catch (Exception exception) {
      GestorLogs.error(GetConstancia.class, "Client Exception", "The client could not be connected", exception);
    }
    return result;
  }
  
  public void getConstanciaController(String[] args) throws Enom151Exception 
  {
	  log.info("lblancas:  [tkn-nom-151] :: "+this.getClass().getName()+".getConstanciaController");
    try {
      if ((args.length != 0) && ((args[0].endsWith("help")) || 
        (args[0].endsWith(String.valueOf('h'))))) {
        showHelp();
        return;
      }
      addArguments(args);
      GestorLogs.info(GetConstancia.class, "main", "Parametros: " + getProperties().toString());
      String dirIn; String dirOut; if ((verifyProperties()) && (validaDatos())) {
        Map<String, byte[]> getConstancias = getConstancia();
        dirIn = getProperties().getProperty("in");
        dirOut = getProperties().getProperty("out");
        for (Map.Entry<String, byte[]> entry : getConstancias.entrySet()) {
          String newDirOut = generateOutputDir(dirOut, (String)entry.getKey());
          String filename = recoverFilename((String)entry.getKey(), dirIn);
          String fileContent = recoverFileContent(filename);
          generatedOutputFromContent(fileContent.getBytes(), filename, null, newDirOut, "GetConstancia", GetConstancia.class);
          generatedOutputFromContent((byte[])entry.getValue(), filename, ".asn", newDirOut, "GetConstancia", GetConstancia.class);
        }
      } else {
        GestorLogs.error(GetConstancia.class, "GetConstancia", Enom151Exception.getTextCode(5));
      }
      deleteInputFiles();
      GestorLogs.info(GetConstancia.class, "main", "Terminated!");
    } catch (IOException ex) {
      GestorLogs.error(GetConstancia.class, "main", ex.getMessage(), ex);
    }
  }
}
